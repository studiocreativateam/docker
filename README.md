# Docker

## Setup
1. Ask me [Jacek Labudda](mailto:j.labudda@creativa.studio?subject=Prosba%20o%20dostep%20do%20repozytoriow) for *key*
2. Run command `composer config --global --editor --auth` and paste *keys*
3. Create project `composer create-project studiocreativateam/docker project_name`
4. Paste *keys* to file `nano docker/auth.json`
5. Copy your ssh key `cp ~/.ssh/id_rsa docker/.ssh/id_rsa` or paste manually
6. Run command `make init-core` or `make up-win`

# Important on production:
### Import DB
#### macOs
`gzcat storage/app/dump/db-backup-[DATE].sql.gz | docker exec -i project_db /usr/bin/psql -U postgres laravel`
#### Linux
`zcat storage/app/dump/db-backup-[DATE].sql.gz | docker exec -i project_db /usr/bin/psql -U postgres laravel`
#### Windows
`zcat storage/app/dump/db-backup-[DATE].sql.gz | docker exec -i project_db psql -U postgres laravel`
### crontab
`* * * * * cd ~/project && make schedule`<br />
`0 * * * * sh /opt/backup-project.sh`

#### Usefully commands
- Docker start: `make up` or `make up-win`
- Docker stop: `make down` or `make down-win`
- Go do bash: `make bash` or `make bash-win`
- Make database backup: `make db-dump`