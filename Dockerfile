FROM serversideup/php:8.2-fpm-nginx

RUN apt-get upgrade
RUN apt-get update --fix-missing
RUN apt-get install -y php8.2-imagick
RUN apt-get install -y php8.2-pgsql
RUN apt-get install -y php8.2-xdebug
RUN apt-get install -y php8.2-swoole
RUN apt-get install -y zip
RUN apt-get install -y vim
RUN apt-get install -y nano
RUN apt-get install -y curl
RUN apt-get install -y gnupg2
RUN apt-get install -y apt-transport-https
RUN apt-get install -y ca-certificates
RUN apt-get install -y software-properties-common
RUN apt-get install -y git
RUN apt-get install -y openssh-client
RUN apt-get install -y build-essential
RUN apt-get install -y supervisor
RUN apt-get install -y cron
RUN apt-get install -y rclone

# docker-cli
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/trusted.gpg.d/docker-archive-keyring.gpg
RUN echo \
      "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
      $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN apt-get update
RUN apt-get install -y docker-ce
RUN apt-get install -y docker-ce-cli
RUN apt-get install -y containerd.io

# composer
RUN curl --silent --show-error https://getcomposer.org/composer.phar > composer.phar \
    && mv composer.phar /usr/bin/composer
RUN chmod +x /usr/bin/composer

# phpunit
RUN curl --location --output /usr/local/bin/phpunit "https://phar.phpunit.de/phpunit-10.phar"
RUN chmod +x /usr/local/bin/phpunit

# xdebug
COPY docker/xdebug.ini /etc/php/current_version/fpm/conf.d/20-xdebug.ini

# opcache
COPY docker/opcache.ini /etc/php/current_version/fpm/conf.d/10-opcache.ini

# cron
RUN crontab -l | { cat; echo "* * * * * php /var/www/html/artisan schedule:run"; } | crontab -
RUN crontab -l | { cat; echo "0 */6 * * * sh /var/www/html/backup.sh"; } | crontab -

# node
RUN curl -fsSL https://deb.nodesource.com/setup_19.x | bash -
RUN apt autoremove
RUN apt-get install -y nodejs
RUN npm install -g npm@latest

# supervisor
COPY docker/supervisord.conf /etc/supervisor/conf.d/supervisor.conf

CMD ["/usr/bin/supervisord", "-n"]