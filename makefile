include .env

init:
	git add --all
	git reset --hard
	git pull
	docker-compose --env-file .env down
	docker exec ${CONTAINER_NAME_APP} sh -c "composer install;php artisan migrate:refresh --force --seed -v;npm install;npm run dev"
update:
	git pull
	docker exec ${CONTAINER_NAME_APP} sh -c "composer install;php artisan migrate; php artisan db:seed --class='SCTeam\Auth\Database\Seeders\PermissionsAndRolesSeeder' --force; npm install;npm run dev"
	make chmod
stage-update:
	git add --all
	git reset --hard
	git pull
	docker exec ${CONTAINER_NAME_APP} sh -c "composer install;php artisan migrate --force;  php artisan db:seed --class='SCTeam\Auth\Database\Seeders\PermissionsAndRolesSeeder' --force;npm install;npm run dev;php artisan optimize"
	make chmod
prod-update:
	git add --all
	git reset --hard
	git pull
	docker exec ${CONTAINER_NAME_APP} sh -c "composer install --no-dev;php artisan migrate --force;  php artisan db:seed --class='SCTeam\Auth\Database\Seeders\PermissionsAndRolesSeeder' --force;npm install;npm run prod;php artisan optimize"
	make chmod
up:
	docker-compose --env-file .env up -d
	docker exec ${CONTAINER_NAME_APP} sh -c "composer install;php artisan migrate;npm install;npm run dev"
	make chmod
down:
	docker-compose down
bash:
	docker exec -it ${CONTAINER_NAME_APP} /bin/bash
queue:
	docker exec ${CONTAINER_NAME_APP} sh -c "php artisan queue:work --tries=3"
schedule:
	docker exec ${CONTAINER_NAME_APP} sh -c "php artisan schedule:run"
db-dump:
	docker exec ${CONTAINER_NAME_POSTGRES} pg_dump -U ${DB_USERNAME} ${DB_DATABASE} --no-owner | gzip -9  > storage/app/dump/db-backup-$(shell date +%F).sql.gz
	find storage/app/dump -mtime +30 -exec rm -rf {} \;
permissions:
	docker exec ${CONTAINER_NAME_APP} sh -c "php artisan db:seed --class='SCTeam\Auth\Database\Seeders\PermissionsAndRolesSeeder' --force"
chmod:
	docker exec -u root ${CONTAINER_NAME_APP} sh -c "chmod -R 777 storage"
run-tests:
	docker exec ${CONTAINER_NAME_APP} sh -c "phpunit --log-junit results/phpunit/phpunit.xml"
package:
	docker exec ${CONTAINER_NAME_APP} sh -c "composer require studiocreativateam/$(package)"
init-win:
	git add --all
	git reset --hard
	git pull
	docker-compose --env-file .env down
	make up-win
update-win:
	git pull
	winpty docker exec ${CONTAINER_NAME_APP} sh -c "composer install;php artisan migrate; php artisan db:seed --class='SCTeam\Auth\Database\Seeders\PermissionsAndRolesSeeder' --force;npm install;npm run dev"
	make chmod
up-win:
	docker-compose --env-file .env up -d
	winpty docker exec ${CONTAINER_NAME_APP} sh -c "composer install;php artisan migrate; php artisan db:seed --class='SCTeam\Auth\Database\Seeders\PermissionsAndRolesSeeder' --force; php artisan cache:clear; php artisan view:clear; php artisan config:clear;npm install;npm run dev"
	make chmod
down-win:
	make down
bash-win:
	winpty docker exec -it ${CONTAINER_NAME_APP} bash
chmod-win:
	winpty docker exec -u root ${CONTAINER_NAME_APP} sh -c "chmod -R 777 storage"
run-tests-win:
	winpty docker exec ${CONTAINER_NAME_APP} sh -c "php artisan test --log-junit results/phpunit/phpunit.xml"
package-win:
	winpty docker exec ${CONTAINER_NAME_APP} sh -c "composer require studiocreativateam/$(package)"
permissions-win:
	winpty docker exec ${CONTAINER_NAME_APP} sh -c "php artisan db:seed --class='SCTeam\Auth\Database\Seeders\PermissionsAndRolesSeeder' --force"